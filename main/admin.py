# -*- coding: utf-8 -*-
from django.contrib import admin
from main.models import Category, Image

# Register your models here.

class CategoryAdmin(admin.ModelAdmin):
	list_display = ['name']

admin.site.register(Category, CategoryAdmin)