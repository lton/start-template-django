# -*- coding: utf-8 -*-
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from datetime import datetime
import mptt
from mptt.models import MPTTModel, TreeForeignKey

# Create your models here.

# Какие-либо категории
class Category(models.Model):
	name = models.CharField(verbose_name = 'Имя категории', max_length = 200, null=True, blank=True)
	description = models.TextField(verbose_name='Описание категории', null=True, blank=True)
	img = models.ImageField(upload_to="media/category/", null=True, blank=True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = u'Категория'
		verbose_name_plural = u'Категории'

# Фотки
class Image(models.Model):
	img = models.ImageField(upload_to="images/", null=True, blank=True)

	class Meta:
		verbose_name = u'Фото'
		verbose_name_plural = u'Фото' 