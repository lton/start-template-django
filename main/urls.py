from django.conf.urls import url

from django.conf import settings
from django.conf.urls.static import static

from main.views import index

urlpatterns = [
    url(r'^$', index, name = 'index'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)