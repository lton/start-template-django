# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-31 20:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_order'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='list',
            new_name='plist',
        ),
    ]
