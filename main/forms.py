# -*- coding: utf-8 -*-
from django.forms import ModelForm
from main.models import Order, Quest

class AppForm(ModelForm):
    class Meta:
        model = Order
        fields = ['name','phone', 'email']

    def __init__(self, *args, **kwargs):
        super(AppForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['id'] = 'name'
        self.fields['name'].widget.attrs['type'] = 'text'
        self.fields['name'].widget.attrs['placeholder'] = 'Имя'
        self.fields['name'].widget.attrs['class'] = 'form-control'

        self.fields['phone'].widget.attrs['id'] = 'phone'
        self.fields['phone'].widget.attrs['type'] = 'text'
        self.fields['phone'].widget.attrs['placeholder'] = 'Телефон'
        self.fields['phone'].widget.attrs['class'] = 'form-control'

        self.fields['email'].widget.attrs['id'] = 'email'
        self.fields['email'].widget.attrs['type'] = 'email'
        self.fields['email'].widget.attrs['placeholder'] = 'Email'
        self.fields['email'].widget.attrs['class'] = 'form-control'

class QuestForm(ModelForm):
    class Meta:
        model = Quest
        fields = ['name', 'email', 'text']

    def __init__(self, *args, **kwargs):
        super(QuestForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['id'] = 'name'
        self.fields['name'].widget.attrs['type'] = 'text'
        self.fields['name'].widget.attrs['placeholder'] = 'Имя'
        self.fields['name'].widget.attrs['class'] = 'form-control'

        self.fields['email'].widget.attrs['id'] = 'email'
        self.fields['email'].widget.attrs['type'] = 'email'
        self.fields['email'].widget.attrs['placeholder'] = 'Email'
        self.fields['email'].widget.attrs['class'] = 'form-control'

        self.fields['text'].widget.attrs['id'] = 'text'
        self.fields['text'].widget.attrs['type'] = 'text'
        self.fields['text'].widget.attrs['class'] = 'form-control'