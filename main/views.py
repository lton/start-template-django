# -*- coding: utf-8 -*-
# Setts import
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.template.loader import get_template
from django.core.mail import send_mail
from django.conf import settings

# Models
from main.models import Category

import random
# Create your views here.

def index(request):
	return render(request, 'index/index.html')
